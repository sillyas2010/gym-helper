import {ObjectID} from 'mongodb';

export type ExerciseT = {
  _id: ObjectID,
  title: string,
  description: string,
  muscleGroup: string,
  muscle: string,
  imageVideo: Array<string>,
  owner: ObjectID
};

export type FormFieldT = {
  [key:string]: string | number | Function | Boolean | Array<string> | Array<AutocompleteVariantT>,
}

type ObjectWithOptionalLink = {
  link?: string,
}

type FileObject = {
  readonly file: File;
  readonly data: string | ArrayBuffer | null;
}

export type FileObjectWithLinkT = FileObject & ObjectWithOptionalLink;

export type DownloadFilesStateT = {
  isDownloading: boolean,
  downloadedFiles: FileObjectWithLinkT[]
}

export type DownloadFilesUseStateT = [
  DownloadFilesStateT,
  Function,
];

export type AutocompleteVariantT = {
  created?: boolean,
  label: string,
  inputValue: string,
}
