import {ObjectId} from 'mongoose';

export const implementation = 'Entity';

export const types = {
  Boolean: {
    type: Boolean,
  },
  Number: {
    type: Number,
  },
  String: {
    type: String,
  },
  Date: {
    type: Date,
  },
  ID: {
    type: ObjectId,
  },
  ObjectId: {
    type: ObjectId,
  },
  Role: {
    type: 'enum',
    enumType: String,
  },
  Subscription: {
    type: 'skip',
  },
  Query: {
    type: 'skip',
  },
  Mutation: {
    type: 'skip',
  },
  __rest__: {
    type: ObjectId,
  },
};

