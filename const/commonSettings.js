module.exports = (PORT, NODE_ENV, devHost, prodHost, gQLPath, gQLWsRoute) => {
  const env = NODE_ENV || 'development',
    isDev = env !== 'production',
    apiPort = PORT || 3000;

  const getHost = (isDev) => {
    const rawHost = isDev ? `${devHost}:${apiPort}` : `${prodHost}`,
      host = isDev ? `http://${rawHost}` : `https://${rawHost}`,
      wsHost = `ws://${rawHost}`;

    return {
      rawHost,
      host,
      wsHost,
    };
  };

  const {
      rawHost,
      host,
      wsHost,
    } = getHost(isDev),
    gQLApiPath = `${host}${gQLPath}`,
    gQLWsPath = `${wsHost}${gQLWsRoute}`,
    appTitle = 'Gym Helper',
    appDescription = 'Gym Helper is an app, which helps you to keep track of your workout progress.',
    appAuthor = '@sillyas2010',
    uploadUrl = 'https://api.imgur.com/3/image',
    uploadCredentials = 'Client-ID df428dfa875a2f9';

  return {
    env,
    isDev,
    apiPort,
    wsHost,
    rawHost,
    host,
    gQLApiPath,
    gQLWsPath,
    appTitle,
    appDescription,
    appAuthor,
    getHost,
    uploadUrl,
    uploadCredentials,
  };
};
