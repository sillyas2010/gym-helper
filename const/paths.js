module.exports = {
  devHost: 'localhost',
  prodHost: 'gym-helper.herokuapp.com',
  gQLPath: '/api/graphql',
  gQLWsRoute: '/api/ws/subscription',
  authPath: '/api/auth',
  authRoute: '/login',
  authLogoutRoute: '/logout',
  authProfileRoute: '/me',
  authCallbackRoute: '/callback',
  publicRoute: '/public',
  swRoute: '/service-worker.js',
  wbRoute: '/workbox-*',
  allRoute: '*',
  pagesByKey: {
    login: '/api/auth/login',
    logout: '/api/auth/logout',
    profile: '/profile',
  },
  defaultFabUrl: '/days/new',
  pages: new Map([
    ['/', {
      title: 'Home',
      withNavigation: true,
    }],
    ['/about', {
      title: 'About',
      withNavigation: true,
      inMore: true,
    }],
    ['/guide', {
      title: 'Installation Guide',
      withNavigation: true,
      inMore: true,
    }],
    ['/exercises', {
      title: 'Exercises',
      withNavigation: true,
      needsAuth: true,
      inMenu: true,
      icon: 'fitness_center',
      fabUrl: '/exercises/new',
    }],
    ['/exercises/new', {
      title: 'New Exercise',
      withNavigation: true,
      fabUrl: '/exercises/new',
    }],
    ['/workout', {
      title: 'Workout',
      inMenu: true,
      icon: 'directions_run',
    }],
    ['/days', {
      title: 'Workout',
      inMore: true,
    }],
    ['/results', {
      title: 'Results',
      inMenu: true,
      icon: 'emoji_events',
    }],
    ['/profile', {
      title: 'Profile',
      withNavigation: true,
    }],
    ['/more', {
      title: 'More',
      inMenu: true,
      icon: 'more_horiz',
    }],
    ['/_error', {
      title: 'Error',
    }],
  ]),
};
