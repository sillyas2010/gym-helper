import getConfig from 'next/config';

import commonSettings from '@const/commonSettings';
import {
  authCallbackRoute, authLogoutRoute, authPath, authProfileRoute, authRoute, defaultFabUrl, devHost,
  gQLPath, gQLWsRoute, pages, pagesByKey, prodHost, publicRoute,
} from '@const/paths';

const {
    PORT,
    NODE_ENV,
  } = getConfig().publicRuntimeConfig,
  {
    env,
    isDev,
    apiPort,
    rawHost,
    host,
    gQLApiPath,
    gQLWsPath,
    appTitle,
    appDescription,
    appAuthor,
    getHost,
    uploadUrl,
    uploadCredentials,
  } = commonSettings(
    PORT,
    NODE_ENV,
    devHost,
    prodHost,
    gQLPath,
    gQLWsRoute,
  );

export {
  env,
  isDev,
  apiPort,
  rawHost,
  host,
  gQLPath,
  gQLApiPath,
  gQLWsPath,
  authPath,
  authRoute,
  authLogoutRoute,
  authProfileRoute,
  authCallbackRoute,
  publicRoute,
  pages,
  pagesByKey,
  defaultFabUrl,
  getHost,
  appTitle,
  appDescription,
  appAuthor,
  uploadUrl,
  uploadCredentials,
};
