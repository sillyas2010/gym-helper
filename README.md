## About

This example shows a minimal example of how to use SSR:

## Usage

#### 1. Install dependencies (within this example's folder)

```bash
yarn
```

#### 2. Start development server

```bash
yarn dev
```

#### 3. Run scaffold task

(with development server running from previous step)

```bash
yarn scaffold
```
