import NProgress from 'nprogress';
import PropTypes from 'prop-types';
import {useEffect} from 'react';

import {useQuery} from '@apollo/react-hooks';
import {currentUser} from '@client/apollo/queries';
import Link from '@components/Link/Link';
import {pagesByKey} from '@const/clientSettings';
import {Box, Button, Typography} from '@material-ui/core';
import {Lock} from '@material-ui/icons';

import {useStyles} from './RestrictedAccessPage.style';

const RestrictedAccessPage = ({needsAuth, children}) => {
  const classes = useStyles(),
    {data, loading} = useQuery(currentUser),
    isSignedIn = !!data && !!data.me && data.me !== null,
    isAccessRestricted = !loading && !isSignedIn && !!needsAuth;

  useEffect(() => {
    if (loading) {
      NProgress.start();
    } else {
      NProgress.done();
    }
  }, [loading]);

  return (
    <>
      { isAccessRestricted ? (
        <Box className={classes.root}>
          <Typography variant="h4" component="h3" className={classes.description}>
            <Lock className={classes.icon}/> <br/>
            To access this page you need to sign in!
          </Typography>
          <Button variant="contained" color="primary" component={Link} naked href={pagesByKey.login}>
            Sign In
          </Button>
        </Box>
      ): children
      }
    </>
  );
};

RestrictedAccessPage.propTypes = {
  needsAuth: PropTypes.bool,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
};

export default RestrictedAccessPage;
