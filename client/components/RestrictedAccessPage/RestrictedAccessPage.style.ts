import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      position: 'fixed',
      top: '50%',
      left: '50%',
      transform: 'translate(-50%, -50%)',
      textAlign: 'center',
    },
    icon: {
      fontSize: '3.5rem',
    },
    description: {
      width: '100%',
      maxWidth: '20rem',
      margin: '0 auto 1rem',
    },
  }),
);
