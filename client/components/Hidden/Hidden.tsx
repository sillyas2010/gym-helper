import PropTypes from 'prop-types';

const Hidden = ({condition = true, isHidden = false, children}) => {
  const isVisible = !condition || isHidden;

  return (
    <>
      {isVisible ?
        children :
        ''
      }
    </>
  );
};

Hidden.propTypes = {
  condition: PropTypes.bool,
  isHidden: PropTypes.bool,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
};

export default Hidden;
