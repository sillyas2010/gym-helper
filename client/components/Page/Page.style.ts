import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    ['@global']: {
      '#nprogress': {
        '& .bar': {
          background: theme.palette.secondary.main,
        },
        '& .peg': {
          boxShadow: `0 0 10px ${theme.palette.secondary.main}, 0 0 5px ${theme.palette.secondary.main}`,
        },
        '& .spinner': {
          right: 'auto',
          left: 15,
        },
        '& .spinner-icon': {
          borderTopColor: theme.palette.secondary.main,
          borderLeftColor: theme.palette.secondary.main,
        },
      },
    },
    paper: {
      backgroundColor: theme.palette.background.default,
      padding: '50px 0 120px',
      minHeight: '100vh',
    },
    hidden: {
      display: 'none',
    },
  }),
);

export {
  useStyles,
};
