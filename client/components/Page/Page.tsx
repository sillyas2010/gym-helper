import clsx from 'clsx';
import Div100vh from 'react-div-100vh';

import withApollo from '@client/hoc/withApollo';
import withDevice from '@client/hoc/withDevice';
import withNavigation from '@client/hoc/withNavigation';
import withOrientation from '@client/hoc/withOrientation';
import {compose, getCurrentPage, isPageAvailable} from '@client/utils';
import PleaseRotate from '@components/PleaseRotate/PleaseRotate';
import RestrictedAccessPage from '@components/RestrictedAccessPage/RestrictedAccessPage';
import {Paper} from '@material-ui/core';

import Branch, {Child} from '../Branch/Branch';
import {useStyles} from './Page.style';

const Page: React.FC = (props: any) => {
  const classes = useStyles(),
    {device, orientation, currentPage, children} = props,
    needsAuth = currentPage && typeof(currentPage.needsAuth) !== 'undefined' ? !!currentPage.withNavigation : false,
    isAvailable = isPageAvailable(device, orientation);

  return (
    <Div100vh style={{minHeight: '100rvh'}} as="main" role="main">
      <Paper square className={classes.paper}>
        <PleaseRotate className={clsx({[classes.hidden]: isAvailable})}/>
        <div className={clsx({[classes.hidden]: !isAvailable})}>
          <RestrictedAccessPage needsAuth={needsAuth}>
            {children}
          </RestrictedAccessPage>
        </div>
      </Paper>
    </Div100vh>
  );
};


const withBranchNavigation = (PageWithProps) => (props) => {
  const {router: {pathname}, device, orientation} = props,
    currentPage = getCurrentPage(pathname),
    needsNav = currentPage && typeof(currentPage.withNavigation) !== 'undefined' ? !!currentPage.withNavigation : true,
    isAvailable = isPageAvailable(device, orientation),
    condition = needsNav && isAvailable,
    PageWithNav = withNavigation(PageWithProps);

  return (
    <RestrictedAccessPage>
      <Branch condition={condition}>
        <Child type="left">
          <PageWithNav {...props} currentPage={currentPage}/>
        </Child>
        <Child type="right">
          <PageWithProps {...props} currentPage={currentPage}/>
        </Child>
      </Branch>
    </RestrictedAccessPage>
  );
};

const PageWithPropsAndNav = compose(
  withDevice,
  withOrientation,
  withApollo({ssr: true}),
  withBranchNavigation,
)(Page);

export default PageWithPropsAndNav;
