import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';

const imageSize = '45vh';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    entityImageWrapper: {
      paddingTop: `calc(${imageSize} - 50px)`,
      [theme.breakpoints.up('sm')]: {
        overflow: 'hidden',
        borderRadius: 20,
        paddingTop: 0,
      },
    },
    entityImage: {
      position: 'absolute',
      left: 0,
      right: 0,
      top: 0,
      width: '100%',
      paddingTop: imageSize,
      background: ({image}: any) => `url(${image}) center/cover no-repeat`,
      [theme.breakpoints.up('sm')]: {
        position: 'static',
        paddingTop: '50%',
      },
    },
  }),
);

export {
  useStyles,
};
