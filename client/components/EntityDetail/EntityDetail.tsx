import Head from 'next/head';
import PropTypes from 'prop-types';

import ClientOnly from '@components/ClientOnly/ClientOnly';
import {Box, Container, Typography} from '@material-ui/core';

import {useStyles} from './EntityDetail.style';

const ExerciseDetail = ({entity, type, image, onRemove, children}) => {
  const classes = useStyles({image}),
    {title, description} = entity;

  return (
    <Container maxWidth="sm">
      { title ? (
        <Head>
          <title>GH | {title} </title>
        </Head>
      ) : (
        <Head>
          <title>GH | {type} </title>
        </Head>
      )
      }
      <ClientOnly>
        {entity?.title ? (
          <Box>
            <Box className={classes.entityImageWrapper}>
              <Box className={classes.entityImage}/>
            </Box>
            <Box>
              <Box my={3}>
                <Typography variant="h3" component="h1" align="center">
                  {title}
                </Typography>
              </Box>
              <Typography variant="h4" color="textSecondary" component="p">
                {description}
              </Typography>
              { children ?
                <Box my={3}>
                  {children}
                </Box> :
                ''
              }
            </Box>
          </Box>
        ) : ''}
      </ClientOnly>
    </Container>
  );
};

ExerciseDetail.propTypes = {
  entity: PropTypes.shape({
    title: PropTypes.string,
    description: PropTypes.string,
  }),
  type: PropTypes.string,
  image: PropTypes.string,
  onRemove: PropTypes.func,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default ExerciseDetail;
