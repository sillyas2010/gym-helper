import clsx from 'clsx';
import NextLink, {LinkProps} from 'next/link';
import {useRouter} from 'next/router';
import PropTypes from 'prop-types';
import React, {ComponentPropsWithoutRef} from 'react';

import MuiLink from '@material-ui/core/Link';

import {useStyles} from './Link.style';

const NextComposed = React.forwardRef(function NextComposed(props: any, ref) {
  const {as, href, ...other} = props;

  return (
    <NextLink href={href} as={as}>
      <a ref={ref} {...other} />
    </NextLink>
  );
});

// A styled version of the Next.js Link component:
// https://nextjs.org/docs/#with-link
function Link(props: any) {
  const {
      href,
      activeClassName = 'active',
      className: classNameProps,
      innerRef,
      naked,
      ...other
    } = props,
    classes = useStyles(),
    router = useRouter(),
    pathname = typeof href === 'string' ? href : href.pathname,
    isCurrent = router.pathname === pathname,
    className = clsx(classes.link, classNameProps, {
      [activeClassName]: isCurrent && activeClassName,
    });

  if (naked) {
    return <NextComposed className={className} ref={innerRef} href={href} {...other} />;
  }

  return (
    <MuiLink component={NextComposed} className={className} ref={innerRef} href={href} {...other} />
  );
}

type AProps = ComponentPropsWithoutRef<'a'>
type CustomLinkProps = {
  naked: Boolean,
};

const StyledLink = React.forwardRef<LinkProps, AProps | CustomLinkProps>((props, ref) => (
  <Link {...props} innerRef={ref} />
));

StyledLink.displayName = 'StyledLink';

export default StyledLink;
