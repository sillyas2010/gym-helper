import {createStyles, Theme, makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    link: {
      ['&:hover']: {
        textDecoration: 'none',
      },
    },
  }),
);

export {
  useStyles,
};
