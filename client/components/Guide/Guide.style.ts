import {createStyles, Theme, makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    guideButton: {
      textAlign: 'center',
      fontFamily: 'Crete Round',
    },
  }),
);

export {
  useStyles,
};
