import React from 'react';

import {useMutation, useQuery} from '@apollo/react-hooks';
import {exercisesList, removeExercise} from '@client/apollo/queries';
import ClientOnly from '@components/ClientOnly/ClientOnly';
import EntityCard from '@components/EntityCard/EntityCard';
import {Container, Grid} from '@material-ui/core';

function Exercises() {
  const {loading, data, refetch} = useQuery(exercisesList),
    [removeExerciseM] = useMutation(removeExercise),
    exercises = !loading && data ? data.exercises : [];

  return (
    <Container maxWidth="md">
      <Grid container spacing={2}>
        <ClientOnly>
          {exercises?.length ? exercises.map((exercise) => (
            <Grid key={exercise.title} item xs={12} sm={6} md={4}>
              <EntityCard entity={exercise} imageUrl={exercise.imageVideo[0]} type="exercises"
                onRemove={async () => {
                  await removeExerciseM({variables: {_id: exercise._id}});
                  refetch();
                }}
              />
            </Grid>
          )) : ''}
        </ClientOnly>
      </Grid>
    </Container>
  );
};

export default Exercises;
