import Head from 'next/head';
import theme from '@client/utils/theme';
import {publicRoute, appTitle, appDescription, appAuthor, getHost} from '@const/clientSettings';

const prodHost = getHost(false);

export default function HeadContent({title}: {title: string}) {
  return (
    <Head>
      <meta httpEquiv="X-UA-Compatible" content="ie=edge"/>
      <meta key="meta_viewport" name="viewport" content="minimum-scale=1, initial-scale=1, maximum-scale=1.0, user-scalable=no, width=device-width, viewport-fit=cover, shrink-to-fit=no"/>
      <meta name="HandheldFriendly" content="true"/>

      <meta name="apple-mobile-web-app-title" content={appTitle}/>
      <meta name="apple-mobile-web-app-capable" content="yes"/>
      <meta name="apple-mobile-web-app-status-bar-style" content="default"/>
      <meta name="mobile-web-app-capable" content="yes"/>
      <meta name="application-name" content={appTitle}/>
      <meta name="description" content={appDescription}/>

      <link rel="manifest" href={`${publicRoute}/site.webmanifest`}/>
      <meta name="msapplication-config" content={`${publicRoute}/browserconfig.xml`}/>

      <link rel="apple-touch-icon" sizes="180x180" href={`${publicRoute}/favicon/apple-touch-icon.png`}/>
      <link rel="icon" type="image/png" sizes="32x32" href={`${publicRoute}/favicon/favicon-32x32.png`}/>
      <link rel="icon" type="image/png" sizes="16x16" href={`${publicRoute}/favicon/favicon-16x16.png`}/>
      <link rel="mask-icon" href={`${publicRoute}/favicon/safari-pinned-tab.svg`} color={theme.palette.primary.main}/>
      <link rel="shortcut icon" href={`${publicRoute}/favicon/favicon.ico`}/>

      <meta name="msapplication-TileColor" content={theme.palette.primary.main}/>
      <meta name="theme-color" content={theme.palette.primary.main}/>

      <meta name="twitter:card" content="summary"/>
      <meta name="twitter:url" content={prodHost.host}/>
      <meta name="twitter:title" content={appTitle}/>
      <meta name="twitter:description" content={appDescription}/>
      <meta name="twitter:image" content={`${prodHost.host}${publicRoute}/favicon/android-chrome-512x512.png`}/>
      <meta name="twitter:creator" content={appAuthor}/>

      <meta property="og:type" content="website"/>
      <meta property="og:title" content={appTitle}/>
      <meta property="og:description" content={appDescription}/>
      <meta property="og:site_name" content={appTitle}/>
      <meta property="og:url" content={prodHost.host}/>
      <meta property="og:image" content={`${prodHost.host}${publicRoute}/favicon/android-chrome-512x512.png`}/>

      <title>{`GH | ${title}`}</title>

      <link
        href="https://fonts.googleapis.com/css2?family=Crete+Round&display=swap"
        rel="stylesheet"/>
      <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"/>
    </Head>
  );
};
