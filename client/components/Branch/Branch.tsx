import PropTypes from 'prop-types';
import {Children} from 'react';

const Child = ({children}) => {
  return children;
};

Child.propTypes = {
  type: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
};

const Branch = ({condition, children}) => {
  const isLeftVisible = !!condition,
    isRightVisible = !condition;

  return (
    <>
      {Children.map(children, (child) => {
        const isLeft = child.props.type === 'left',
          isRight = child.props.type === 'right',
          isVisible = isLeft ? isLeftVisible :
            (isRight ? isRightVisible : false);

        if (!isVisible) return;

        return child;
      })}
    </>
  );
};

Branch.propTypes = {
  condition: PropTypes.bool,
  isHidden: PropTypes.bool,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
};

export default Branch;

export {
  Child,
};
