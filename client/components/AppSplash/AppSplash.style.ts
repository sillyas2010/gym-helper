import {createStyles, Theme, makeStyles} from '@material-ui/core/styles';

const fontSize = '12vh',
  useStyles = makeStyles((theme: Theme) =>
    createStyles({
      appTitle: {
        fontSize,
        textAlign: 'center',
        fontFamily: 'Crete Round',
      },
      helper: {
        display: 'inline-block',
      },
      helperIcon: {
        color: theme.palette.secondary.main,
        height: fontSize,
        width: fontSize,
        transform: 'translateY(25%)',
      },
      guideButton: {
        position: 'absolute',
        bottom: 80,
        right: 10,
      },
    }),
  );

export {
  useStyles,
};
