import HelperIcon from '@components/HelperIcon/HelperIcon';
import Link from '@components/Link/Link';
import {Icon} from '@material-ui/core';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';

import {useStyles} from './AppSplash.style';

export default function AppSplash() {
  const classes = useStyles();

  return (
    <Container maxWidth="sm">
      <Box>
        <Typography variant="h4" component="h1" gutterBottom className={classes.appTitle}>
          Gym <br/><span className={classes.helper}><HelperIcon className={classes.helperIcon}/>elper</span>
        </Typography>
        {/*
          // @ts-ignore */}
        <Button
          className={classes.guideButton}
          variant="contained"
          color="primary"
          endIcon={<Icon>help_outline</Icon>}
          component={Link} naked href="/guide">
          Guide
        </Button>
      </Box>
    </Container>
  );
};
