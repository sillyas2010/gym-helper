import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formTitle: {
      textAlign: 'center',
      marginBottom: theme.spacing(2),
    },
    fullWidth: {
      width: '100%',
    },
    fieldWrapper: {
      paddingTop: theme.spacing(1),
      paddingBottom: theme.spacing(1),
      width: '100%',
    },
    requiredStar: {
      marginLeft: 2,
      fontSize: '1rem',
    },
    submitWrapper: {
      position: 'relative',
    },
    formSubmit: {
      display: 'block',
      marginLeft: 'auto',
      marginRight: 'auto',
      marginTop: theme.spacing(3),
      height: '3.5rem',
      minWidth: '15rem',
      maxWidth: '100%',
      ['&.Mui-disabled']: {
        cursor: 'not-allowed',
        pointerEvents: 'auto',
        opacity: .5,
      },
    },
    submittingProgress: {
      position: 'absolute',
      top: '50%',
      left: '50%',
      marginTop: -12,
      marginLeft: -12,
      cursor: 'not-allowed',
    },
    uploadButton: {
      height: '3.5rem',
      width: '100%',
      textTransform: 'none',
    },
    uploadButtonError: {
      borderColor: theme.palette.error.main,
      ['& .MuiButton-label']: {
        color: theme.palette.error.main,
      },
    },
    fieldStyling: {
      ['& .MuiInputLabel-root.Mui-focused:not(.Mui-error)']: {
        color: theme.palette.common.white,
      },
      ['& .MuiSvgIcon-root']: {
        fill: theme.palette.common.white,
      },
      ['& .MuiOutlinedInput-root.Mui-focused:not(.Mui-error) .MuiOutlinedInput-notchedOutline']: {
        borderColor: theme.palette.common.white,
      },
    },
    shrinkedSelect: {
      ['& .MuiInputBase-root .MuiOutlinedInput-notchedOutline legend']: {
        maxWidth: 1000,
      },
    },
  }),
);
