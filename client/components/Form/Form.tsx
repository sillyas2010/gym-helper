import clsx from 'clsx';
import {useFormik} from 'formik';
import {DropzoneDialogBase} from 'material-ui-dropzone';
import PropTypes from 'prop-types';
import {useState} from 'react';

import {stringifyObjectInputValue} from '@client/utils';
import ClientOnly from '@components/ClientOnly/ClientOnly';
import {uploadCredentials, uploadUrl} from '@const/clientSettings';
import {AutocompleteVariantT} from '@const/types';
import {
  Button, Chip, CircularProgress, FormControl, FormHelperText, Icon, InputLabel, Select, Snackbar,
  TextField, Typography,
} from '@material-ui/core';
import MuiAlert, {AlertProps} from '@material-ui/lab/Alert';
import Autocomplete, {createFilterOptions} from '@material-ui/lab/Autocomplete';

import {useStyles} from './Form.style';

const Alert = (props: AlertProps) => {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  },
  filter = createFilterOptions<AutocompleteVariantT>(),
  sendFile = async (fileObj, key) => {
    if (fileObj.link) {
      return {...fileObj, key};
    }

    const myHeaders = new Headers();
    myHeaders.append('Authorization', uploadCredentials);

    const formData = new FormData(),
      type = fileObj.file.type.startsWith('video/') ? 'video' : 'image',
      fileTitle = fileObj.file.name.split('.').shift(),
      splitDataMarker = ';base64,';
    let fileData;

    if (type === 'image') {
      const indexOfMarker = fileObj.data.indexOf(splitDataMarker),
        indexOfDataStart = indexOfMarker ? indexOfMarker + splitDataMarker.length : 0;

      fileData = fileObj.data.substr(indexOfDataStart);
      formData.append('type', 'base64');
    } else if (type === 'video') {
      formData.append('type', 'file');
      const res = await fetch(fileObj.data);
      fileData = await res.blob();
    }
    formData.append(type, fileData);
    formData.append('name', fileObj.file.name);
    formData.append('title', fileTitle);

    const requestOptions: any = {
      method: 'POST',
      headers: myHeaders,
      body: formData,
      redirect: 'follow',
    };

    try {
      const res = await fetch(uploadUrl, requestOptions),
        data = await res.json();

      if (data?.data?.link) {
        return {...fileObj, link: data.data.link, key};
      } else {
        throw Error('Response has no link');
      }
    } catch (err) {
      throw Error(err);
    }
  },
  uploadMedia = async (values) => {
    const mediaArray = (Object.keys(values)).reduce((resArr: Array<any>, key) => {
      const currentEl = values[key];
      if (Array.isArray(currentEl)) {
        currentEl.forEach((possibleFile) => {
          if (typeof possibleFile === 'object' && possibleFile.type === 'file') {
            resArr.push(sendFile(possibleFile, key));
          }
        });
      }
      return resArr;
    }, []);

    try {
      const completedRequests = await Promise.all(mediaArray),
        uploadLinks = {},
        uploadValues = completedRequests.reduce((valObj, resultObj) => {
          const {key} = resultObj,
            currentValue = valObj[key];
          if (currentValue && Array.isArray(currentValue)) {
            uploadLinks[key].push(resultObj.link);
            valObj[key].push(resultObj);
          } else {
            uploadLinks[key] = [resultObj.link];
            valObj[key] = [resultObj];
          }

          return valObj;
        }, {});

      return [uploadValues, uploadLinks];
    } catch (err) {
      throw Error(err);
    }
  },
  defaultValidate = (values, field) => {
    const currentValue = values[field.key];

    if (field.required) {
      if ((Array.isArray(currentValue) && currentValue.length === 0) || !currentValue) {
        return `${field.label} is required`;
      }
    }
  },
  defaultAlertStatus: any = {
    opened: false,
    type: undefined,
    message: '',
  };

const Form = ({formFields, title, isTouched, onSubmit}) => {
  const initialTouched = {},
    initialValues = formFields.reduce(
      (fieldsObj, {key, initialValue, onInit}) => {
        const initValue = initialValue || '';

        fieldsObj[key] = onInit ? onInit(initValue) : initValue;
        initialTouched[key] = !!isTouched;

        return fieldsObj;
      },
      {},
    ),
    [alertStatus, setAlertStatus] = useState(defaultAlertStatus),
    closeAlert = () => setAlertStatus(defaultAlertStatus),
    validate = (values) => formFields.reduce((errors, field) => {
      const defaultError = defaultValidate(values, field);

      if (defaultError) {
        errors[field.key] = defaultError;
      }

      if (field.validate && values[field.key] !== undefined) {
        const possibleErr = field.validate(values, field.key);

        if (possibleErr) {
          errors[field.key] = possibleErr;
        }
      }

      return errors;
    }, {}),
    onDefaultSubmit = async (values, formikBag) => {
      if (onSubmit) {
        const [uploadValues, uploadLinks] = await uploadMedia(values),
          fieldsWithUpload = Object.keys(uploadValues),
          stringifiedValues = stringifyObjectInputValue(values);

        if (fieldsWithUpload.length) {
          fieldsWithUpload.forEach((fieldKey) => {
            formikBag.setFieldValue(fieldKey, uploadValues[fieldKey]);
          });
        }

        const completedValues = {...stringifiedValues, ...uploadLinks},
          result = await onSubmit(completedValues, formikBag);

        if (result.error) {
          setAlertStatus({opened: true, type: 'error', message: result.error});
        } else {
          setAlertStatus({opened: true, type: 'success', message: result.message});
        }
      }
    },
    formik = useFormik({
      initialValues,
      initialTouched: isTouched ? initialTouched : undefined,
      validate,
      onSubmit: onDefaultSubmit,
    }),
    [dialogs, setDialogs] = useState({}),
    toggleDialog = (key) => setDialogs({...dialogs, [key]: !dialogs[key]}),
    classes = useStyles();

  return (
    <div>
      <Typography variant="h4" component="h2" className={classes.formTitle}>
        {title}
      </Typography>
      <form noValidate autoComplete="off" onSubmit={formik.handleSubmit}>
        {
          formFields.map((field) => {
            const fieldErr = formik.errors[field.key],
              fieldTouched = formik.touched[field.key],
              hasError = !!fieldErr && !!fieldTouched,
              type = field.type || 'text',
              onFieldChange: any = formik.handleChange(field.key),
              onFieldBlur: any = formik.handleBlur(field.key),
              errMessage = (<Typography component="span">{fieldErr}</Typography>),
              currentValue = formik.values[field.key],
              limitReached = field.limit && Array.isArray(currentValue) &&
                currentValue.length >= field.limit,
              shrinkedSelect = true;

            switch (type) {
            case 'select':
              return (
                <div key={field.id} className={classes.fieldWrapper}>
                  <FormControl variant="outlined" required={field.required} error={hasError}
                    className={clsx(
                      classes.fullWidth, classes.fieldStyling, {[classes.shrinkedSelect]: shrinkedSelect},
                    )}
                  >
                    <InputLabel id={`${field.id}-label`} shrink={shrinkedSelect}>{field.label}</InputLabel>
                    <Select
                      native
                      multiple={field.multiple}
                      labelId={`${field.id}-label`}
                      id={field.id}
                      label={field.label}
                      value={formik.values[field.key]}
                      onChange={(e) => (onFieldBlur(e), onFieldChange(e))}
                    >
                      <option aria-label="None" value="">None</option>
                      <ClientOnly>
                        { field?.variants?.length ?
                          field.variants.map((variant, ind) => (
                            <option key={`${variant.inputValue}_${ind}`} value={variant.inputValue}>
                              {variant.label ? variant.label : variant.inputValue}
                            </option>
                          )) : ''
                        }
                      </ClientOnly>
                    </Select>
                    { hasError ?
                      (<FormHelperText>
                        {errMessage}
                      </FormHelperText>) :
                      ''
                    }
                  </FormControl>
                </div>
              );
            case 'autocomplete':
              return (
                <div key={field.id} className={classes.fieldWrapper}>
                  <FormControl error={hasError} className={classes.fullWidth}>
                    <Autocomplete
                      selectOnFocus
                      freeSolo
                      id={field.id}
                      multiple={field.multiple}
                      value={formik.values[field.key]}
                      options={field.variants}
                      renderOption={(option) => option.created ? `Add "${option.label}"` : option.label}
                      onChange={(_, newValue: any) => {
                        if (field.multiple) {
                          const newValueArr = Array.isArray(newValue) ? newValue.map((el) => (
                            el.inputValue ? el : {inputValue: el, label: el, created: true}
                          )) : [];

                          formik.setFieldValue(field.key, newValueArr);
                        } else {
                          formik.setFieldValue(field.key, newValue ? (newValue.inputValue || '') : '');
                        }
                      }}
                      getOptionDisabled={limitReached ? ((option) =>
                        option.inputValue === 'reached_limit'
                      ) : undefined}
                      filterOptions={(options, params) => {
                        const filtered = filter(options, params),
                          hasCreatable = field.creatable && params.inputValue !== '',
                          moreThanLimit = limitReached && params.inputValue !== '';

                        if (moreThanLimit) {
                          filtered.push({
                            inputValue: 'reached_limit',
                            label: `Maximum number of ${field.label} reached`,
                          });
                        } else if (hasCreatable) {
                          filtered.push({
                            inputValue: params.inputValue,
                            label: params.inputValue,
                            created: true,
                          });
                        }

                        return filtered;
                      }}
                      getOptionLabel={(option: AutocompleteVariantT) => {
                        if (Array.isArray(option) && option.length === 0) {
                          return '';
                        }
                        if (typeof option === 'string') {
                          return option;
                        }
                        return option.inputValue;
                      }}
                      renderTags={(value, getTagProps) =>
                        value.map((option, index) => (
                          <Chip
                            key={`${option.inputValue}_${index}`}
                            variant="outlined"
                            label={option.label}
                            {...getTagProps({index})}
                          />
                        ))
                      }
                      renderInput={(params: any) => (
                        <TextField {...params} label={field.label}
                          error={hasError}
                          required={field.required}
                          className={classes.fieldStyling}
                          variant="outlined"
                          inputProps={{
                            ...params.inputProps,
                            onBlur: (...args) => (
                              onFieldBlur(...args),
                              params.inputProps.onBlur(...args)
                            ),
                          }}
                        />
                      )}
                    />
                    { hasError ?
                      <FormHelperText variant="filled">
                        {errMessage}
                      </FormHelperText> :
                      ''
                    }
                  </FormControl>
                </div>
              );
            case 'media':
              return (
                <div key={field.id} className={classes.fieldWrapper}>
                  <FormControl variant="outlined" required={field.required}
                    error={hasError}
                    className={classes.fullWidth}
                  >
                    <Button variant="outlined"
                      onClick={() => toggleDialog(field.key)}
                      className={clsx(classes.uploadButton, {[classes.uploadButtonError]: hasError})}
                      endIcon={<Icon>cloud_upload</Icon>}
                    >
                      Upload {field.label} {field.required ?
                        <span className={classes.requiredStar}>*</span> :
                        ''
                      }
                    </Button>
                    { hasError ?
                      <FormHelperText variant="filled">
                        {errMessage}
                      </FormHelperText> :
                      ''
                    }
                  </FormControl>
                  <DropzoneDialogBase
                    acceptedFiles={field.accepted}
                    fileObjects={formik.values[field.key]}
                    cancelButtonText="Cancel"
                    submitButtonText="Save"
                    maxFileSize={10485760}
                    open={dialogs[field.key]}
                    showPreviews={true}
                    showFileNamesInPreview={true}
                    dialogTitle={`Upload ${field.label}`}
                    onAdd={(newFileObjs: any) => {
                      const newFiles = newFileObjs.map((file) => ({
                          ...file,
                          type: 'file',
                        })),
                        allFiles = [].concat(formik.values[field.key], newFiles);
                      formik.setFieldValue(field.key, allFiles);
                    }}
                    onDelete={(delFileObj) => {
                      const lastIndex = (formik.values[field.key]).findIndex(
                          (el) => el.file.name === delFileObj.file.name,
                        ),
                        newFiles = [...formik.values[field.key]];

                      newFiles.splice(lastIndex, 1);
                      formik.setFieldValue(field.key, newFiles);
                    }}
                    onClose={(e) => {
                      onFieldBlur(e);
                      toggleDialog(field.key);
                    }}
                    onSave={(e) => {
                      onFieldBlur(e);
                      toggleDialog(field.key);
                    }}
                  />
                </div>
              );
            case 'text':
            default:
              return (
                <div key={field.id} className={classes.fieldWrapper}>
                  <TextField
                    className={clsx(classes.fullWidth, classes.fieldStyling)}
                    required={field.required}
                    error={hasError}
                    id={field.id}
                    label={field.label}
                    placeholder={field.placeholder}
                    helperText={hasError ? errMessage : ''}
                    variant="outlined"
                    value={formik.values[field.key]}
                    onChange={onFieldChange}
                    inputProps={{
                      onBlur: onFieldBlur,
                    }}
                  />
                </div>
              );
            }
          })
        }
        <div className={classes.submitWrapper}>
          <Button type="submit" variant="outlined"
            disabled={formik.isValidating || formik.isSubmitting}
            className={classes.formSubmit}
          >
            Submit
          </Button>
          {formik.isSubmitting &&
            <CircularProgress size={24} color="secondary" className={classes.submittingProgress} />
          }
        </div>
      </form>
      <Snackbar open={alertStatus.opened} autoHideDuration={6000} onClose={closeAlert}>
        <Alert onClose={closeAlert} severity={alertStatus.type}>
          {alertStatus.message}
        </Alert>
      </Snackbar>
    </div>
  );
};

Form.propTypes = {
  formFields: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
    key: PropTypes.string,
    label: PropTypes.string,
    placeholder: PropTypes.string,
    type: PropTypes.string,
    validate: PropTypes.func,
    required: PropTypes.bool,
    multiple: PropTypes.bool,
    creatable: PropTypes.bool,
    initialValue: PropTypes.any,
    initialFiles: PropTypes.arrayOf(PropTypes.instanceOf(File)),
    variants: PropTypes.arrayOf(PropTypes.shape({
      value: PropTypes.string,
      label: PropTypes.string,
    })),
  })),
  title: PropTypes.string,
  isTouched: PropTypes.bool,
  onSubmit: PropTypes.func,
};

export default Form;
