import {useEffect, useState} from 'react';

const ClientOnly = ({children}: any) => {
  const [isVisible, setVisible] = useState(false);

  useEffect(() => {
    setVisible(true);
  }, []);

  return (
    <>
      {isVisible ?
        children :
        ''
      }
    </>
  );
};

export default ClientOnly;
