import {useRouter} from 'next/router';
import {useMemo} from 'react';

import {useQuery} from '@apollo/react-hooks';
import {currentUser} from '@client/apollo/queries';
import Link from '@components/Link/Link';
import {defaultFabUrl, pages, pagesByKey} from '@const/clientSettings';
import AppBar from '@material-ui/core/AppBar';
import Fab from '@material-ui/core/Fab';
import Toolbar from '@material-ui/core/Toolbar';
import AddIcon from '@material-ui/icons/Add';
import PersonIcon from '@material-ui/icons/Person';

import {useStyles} from './Navigation.style';
import NavigationItem from './NavigationItem';

const isActiveUrl = (pathname: string, url: string) => (
  pathname === url || (url !== '/' && pathname.startsWith(url))
);

const getCurrentNav = (pages, pathname: string) => {
  const pagesEntries = [...pages.entries()],
    fabUrl = pagesEntries.reduce((resUrl, [url, page]) =>
      (isActiveUrl(pathname, url) && page.fabUrl ? resUrl = page.fabUrl : resUrl)
    , ''),
    origNav = pagesEntries.filter(([_, page]) => page.inMenu);

  const navMarkup = origNav.map(([url, page], __, items) => (
    <NavigationItem count={items.length} page={page} href={url} isActive={isActiveUrl(pathname, url)} key={url}/>
  ));

  const halfLength = Math.ceil(navMarkup.length / 2),
    leftSide = navMarkup.slice(0, halfLength),
    rightSide = navMarkup.slice(halfLength);

  return {
    fabUrl: fabUrl || defaultFabUrl,
    leftSide,
    rightSide,
  };
};

const Navigation = () => {
  const classes = useStyles(),
    {pathname} = useRouter(),
    {loading, data} = useQuery(currentUser),
    signedIn = !loading && data && data.me !== null,
    navigation = useMemo(() => getCurrentNav(pages, pathname), [pathname]);

  return (
    <>
      <Link naked={true} href={signedIn ? pagesByKey.profile : pagesByKey.login}>
        <Fab size="medium" color={signedIn ? 'secondary' : undefined}
          className={classes.userFab} aria-label="Sign In"
        >
          <PersonIcon/>
        </Fab>
      </Link>
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <div className={classes.rounded}>
            <div className={classes.corners}/>
          </div>
          <nav className={classes.nav}>
            <div className={classes.grow} />

            {navigation['leftSide']}

            <div className={classes.grow} />
            <Fab href={navigation.fabUrl} component={Link} color="secondary" aria-label="add" className={classes.fabButton}>
              <AddIcon className={classes.fabButtonIcon}/>
            </Fab>

            {navigation['rightSide']}

            <div className={classes.grow} />
          </nav>
        </Toolbar>
      </AppBar>
    </>
  );
};

export default Navigation;
