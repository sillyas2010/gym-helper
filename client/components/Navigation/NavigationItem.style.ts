import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';

const useStyles = makeStyles(({palette: {secondary, common, text}}: Theme) =>
  createStyles({
    button: {
      ['& .MuiButton-label *']: {
        color: text.secondary,
      },
      ['&:hover']: {
        ['& .MuiButton-label *']: {
          color: secondary.light,
        },
      },
    },
    activeButton: {
      ['& .MuiButton-label *']: {
        color: secondary.main,
      },
    },
    buttonContent: {
      display: 'flex',
      flexDirection: 'column',
      flexWrap: 'wrap',
      alignItems: 'center',
    },
    icon: {
      fontSize: '2rem',
    },
    title: {
      minWidth: '4.5rem',
      fontSize: '.8rem',
      textAlign: 'center',
      textTransform: 'capitalize',
    },
  }),
);

export {
  useStyles,
};
