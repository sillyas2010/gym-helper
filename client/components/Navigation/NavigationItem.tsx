import clsx from 'clsx';
import {bool, number, object, string} from 'prop-types';

import Link from '@components/Link/Link';
import {Button, Icon} from '@material-ui/core';

import {useStyles} from './NavigationItem.style';

const NavigationItem = ({page, href, isActive, count}) => {
  const classes = useStyles({count});

  return (
    <Button href={href} component={Link} className={clsx(classes.button, {[classes.activeButton]: isActive})}>
      <div className={classes.buttonContent}>
        <Icon className={classes.icon}>{page.icon}</Icon>
        <span className={classes.title}>
          {page.navigationTitle || page.title}
        </span>
      </div>
    </Button>
  );
};

NavigationItem.propTypes = {
  count: number,
  href: string,
  isActive: bool,
  page: object,
};

export default NavigationItem;
