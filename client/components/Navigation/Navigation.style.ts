import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';

const fabSize = 64,
  cornerSize = 5,
  cornerX = (isBefore) => `calc(50% ${isBefore ? '-' : '+'} ${fabSize/2}px)`,
  cornerWidth = (isBefore) => (isBefore ?
    `0 ${cornerSize}px ${cornerSize}px 0` :
    `${cornerSize}px ${cornerSize}px 0 0`
  ),
  cornerTransform = (isBefore) => (
    `${isBefore ? 'translateX(-100%)' : ''} ` +
    `rotate(360deg)`
  ),
  cornerColor = (isBefore, color) => (
    `${isBefore ? 'transparent' : color} ${isBefore ? color : 'transparent'} transparent transparent`
  ),
  useStyles = makeStyles((theme: Theme) =>
    createStyles({
      appBar: {
        backgroundColor: 'transparent',
        boxShadow: 'none',
        top: 'auto',
        bottom: 0,
      },
      nav: {
        display: 'flex',
        width: '100%',
      },
      rounded: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        overflow: 'hidden',
        ['&:before']: {
          content: `''`,
          position: 'absolute',
          top: `-${fabSize/2}px`,
          left: '50%',
          transform: 'translateX(-50%)',
          borderRadius: '1000px',
          width: `${fabSize}px`,
          height: `${fabSize}px`,
          boxShadow: `0 0 0 5000px ${theme.palette.primary.main}`,
          zIndex: -1,
        },
      },
      corners: {
        ['&:before, &:after']: {
          content: `''`,
          position: 'absolute',
          top: 0,
          width: 0,
          height: 0,
          borderStyle: 'solid',
          zIndex: -1,
        },
        ['&:before']: {
          left: cornerX(true),
          transform: cornerTransform(true),
          borderWidth: cornerWidth(true),
          borderColor: cornerColor(true, theme.palette.background.default),
        },
        ['&:after']: {
          left: cornerX(false),
          transform: cornerTransform(false),
          borderWidth: cornerWidth(false),
          borderColor: cornerColor(false, theme.palette.background.default),
        },
      },
      grow: {
        flexGrow: 1,
      },
      userFab: {
        position: 'fixed',
        top: theme.spacing(2),
        right: theme.spacing(2),
        zIndex: 1,
      },
      fabButton: {
        position: 'absolute',
        zIndex: 1,
        top: -30,
        left: 0,
        right: 0,
        margin: '0 auto',
      },
      fabButtonIcon: {
        color: theme.palette.text.primary,
      },
    }),
  );

export {
  useStyles,
};
