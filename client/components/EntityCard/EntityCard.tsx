import PropTypes from 'prop-types';
import React from 'react';

import Link from '@components/Link/Link';
import {
  Button, Card, CardActions, CardContent, CardMedia, IconButton, Typography,
} from '@material-ui/core';
import {Delete, Edit, MoreHoriz} from '@material-ui/icons';

import {useStyles} from './EntityCard.style';

const EntityCard = ({entity, imageUrl, type, onRemove}) => {
  const classes = useStyles(),
    onEntityRemove = () => onRemove(entity._id);

  return (
    <Card key={entity.title} className={classes.root}>
      <Button href={`/${type}/${entity._id}`} className={classes.link}
        component={Link}
      >
        <CardMedia
          className={classes.media}
          image={imageUrl}
          title={entity.title}
        />
        <CardContent className={classes.content}>
          <Typography gutterBottom variant="h5" color="textPrimary" component="h2">
            {entity.title}
          </Typography>
          { entity.description ?
            <Typography variant="body2" color="textPrimary" component="p" className={classes.description}>
              {entity.description}
            </Typography> :
            ''
          }
        </CardContent>
      </Button>
      <CardActions className={classes.actions}>
        <IconButton size="small" className={classes.actionButtons} onClick={onEntityRemove}>
          <Delete/>
        </IconButton>
        <IconButton size="small" href={`/${type}/${entity._id}/edit`} className={classes.actionButtons} component={Link}>
          <Edit/>
        </IconButton>
        <IconButton size="small" href={`/${type}/${entity._id}`} className={classes.actionButtons} component={Link}>
          <MoreHoriz/>
        </IconButton>
      </CardActions>
    </Card>
  );
};

EntityCard.propTypes = {
  entity: PropTypes.shape({
    _id: PropTypes.string,
    title: PropTypes.string,
    description: PropTypes.string,
  }),
  imageUrl: PropTypes.string,
  onRemove: PropTypes.func,
  type: PropTypes.string,
};

export default EntityCard;
