import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      height: '100%',
      display: 'flex',
      flexDirection: 'column',
    },
    link: {
      padding: 0,
      flex: 1,
      borderRadius: 0,
      ['& .MuiButton-label']: {
        display: 'flex',
        flexDirection: 'column',
        height: '100%',
        textTransform: 'none',
      },
    },
    media: {
      flex: '0 0 auto',
      width: '100%',
      paddingTop: '50%',
      clipPath: 'polygon(0 0, 100% 0, 100% 100%, 0 80%)',
    },
    content: {
      flex: 2,
      width: '100%',
      marginTop: '-12%',
      transform: 'translateX(0)',
      textShadow: '1px 1px 2px rgba(0,0,0, 0.75)',
    },
    description: {
      maxHeight: '3.75rem',
      overflow: 'hidden',
    },
    actions: {
      justifyContent: 'flex-end',
    },
    actionButtons: {
      color: theme.palette.common.white,
    },
  }),
);
