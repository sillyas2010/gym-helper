import Link from '@components/Link/Link';
import {publicRoute} from '@const/clientSettings';
import Button from '@material-ui/core/Button';

import {useStyles} from './NotFound.style';

export default function NotFound() {
  const classes = useStyles();

  return (
    <div className={classes.notFoundWrapper}>
      <div className={classes.imageWrapper}>
        <img className={classes.image}
          src={`${publicRoute}/img/404.svg`}
          alt="404-not-found"
        />
      </div>
      <h1 className={classes.textAlignCenter}>
        Page not found
      </h1>
      <div className={classes.textAlignCenter}>
        {/*
          // @ts-ignore */}
        <Button color="primary" variant="contained" component={Link} naked href={'/'}>
          Go Home!
        </Button>
      </div>
    </div>
  );
};
