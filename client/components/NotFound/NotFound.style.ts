import {createStyles, Theme, makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    notFoundWrapper: {
      position: 'absolute',
      top: '50%',
      left: '50%',
      transform: 'translate(-50%,-50%)',
      padding: '0 3rem',
      [theme.breakpoints.down('xs')]: {
        padding: '0',
      },
    },
    imageWrapper: {
      maxWidth: '100%',
    },
    image: {
      maxWidth: '100%',
    },
    textAlignCenter: {
      textAlign: 'center',
    },
  }),
);

export {
  useStyles,
};
