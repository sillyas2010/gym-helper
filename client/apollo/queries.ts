import gql from 'graphql-tag';

export const exercisesList = gql`
  query {
    exercises {
      _id
      title
      description
      muscleGroup
      muscle
      imageVideo
    }
  }
`;

export const exerciseDetail = gql`
  query Exercise($_id: String!) {
    exercise(_id: $_id) {
      _id
      title
      description
      muscleGroup
      muscle
      imageVideo
    }
  }
`;

export const currentUser = gql`
  query {
    me {
      _id
      email
      login
      name
    }
  }
`;

export const addExercise = gql`
  mutation AddExercise($title: String!, $description: String, $muscleGroup: String!, $muscle: String!, $imageVideo: [String]!) {
    addExercise(title: $title, description: $description, muscleGroup: $muscleGroup, muscle: $muscle, imageVideo: $imageVideo) {
      _id
    }
  }
`;

export const editExercise = gql`
  mutation EditExercise($_id: String!, $title: String, $description: String, $muscleGroup: String, $muscle: String!, $imageVideo: [String]) {
    editExercise(_id: $_id, title: $title, description: $description, muscleGroup: $muscleGroup, muscle: $muscle, imageVideo: $imageVideo) {
      _id
    }
  }
`;

export const removeExercise = gql`
  mutation RemoveExercise($_id: String!) {
    removeExercise(_id: $_id) {
      _id
    }
  }
`;
