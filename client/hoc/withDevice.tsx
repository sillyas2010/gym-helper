import {useState, useEffect} from 'react';
import isMobileLib from 'ismobilejs';

const withDevice = (Component) => (props) => {
  const isClient = typeof window !== 'undefined',
    [isMobileDevice, setMobileDevice]: Array<any> = useState(isClient ? isMobileLib(window.navigator) : {}),
    isMobile = !!isMobileDevice.any,
    isDesktop = !isMobile;

  useEffect(() => {
    const currentDevice = isMobileLib(window.navigator);

    setMobileDevice(currentDevice);
  }, []);

  return (
    <Component {...props} device={{isMobile, isMobileDevice, isDesktop}}/>
  );
};

export default withDevice;
