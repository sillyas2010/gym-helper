import Navigation from '@components/Navigation/Navigation';

const withNavigation = (Page) => (props) => {
  return (
    <>
      <Navigation/>
      <Page {...props}/>
    </>
  );
};

export default withNavigation;
