import {useState, useEffect} from 'react';

const getOrientation = () =>
  typeof window !== 'undefined' ? screen.orientation.type : 'portrait-primary';

const withOrientation = (Component) => (props) => {
  const [orientation, setOrientation] =
    useState(getOrientation()),
    isLandscape = orientation === 'landscape-primary' || orientation === 'landscape-secondary',
    isPortrait = !isLandscape;

  const updateOrientation = (event) => {
    setOrientation(getOrientation());
  };

  useEffect(() => {
    window.addEventListener(
      'orientationchange',
      updateOrientation,
    );
    return () => {
      window.removeEventListener(
        'orientationchange',
        updateOrientation,
      );
    };
  }, []);

  return (
    <Component {...props} orientation={{orientation, isPortrait, isLandscape}}/>
  );
};

export default withOrientation;
