import {ApolloClient, HttpLink, InMemoryCache, NormalizedCacheObject} from 'apollo-boost';
import fetch from 'isomorphic-fetch';
import {NextPageContext} from 'next';

import {gQLApiPath} from '@const/clientSettings';

export const createApolloClient = (initialState = {}, ctx: NextPageContext | undefined): ApolloClient<NormalizedCacheObject> => {
  // The `ctx` (NextPageContext) will only be present on the server.
  // use it to extract auth headers (ctx.req) or similar.
  return new ApolloClient({
    ssrMode: !!ctx,
    link: new HttpLink({
      uri: gQLApiPath, // Server URL (must be absolute)
      credentials: 'same-origin', // Additional fetch() options like `credentials` or `headers`
      fetch,
      headers: ctx?.req?.headers,
    }),
    cache: new InMemoryCache({
      dataIdFromObject: (object) => object.id,
    }).restore(initialState || {}),
  });
};
