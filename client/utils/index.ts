import {pages} from '@const/clientSettings';
import {FileObjectWithLinkT} from '@const/types';

export const compose = (...funcs) =>
  funcs.reduce((a, b) => (...args) => a(b(...args)), (arg) => arg);

export const toBase64 = (file) => new Promise((resolve, reject) => {
  const reader = new FileReader();
  reader.readAsDataURL(file);
  reader.onload = () => resolve(reader.result);
  reader.onerror = (error) => reject(error);
});

export const styledBy = (property: string, mapping) => (props) => {
  return mapping[props[property]];
};

export const getCurrentPage = (pathname) => {
  const pagesEntries = [...pages.entries()],
    currentPage = pagesEntries.find(([url, page]) => pathname === url || (url !== '/' && pathname.startsWith(url)));

  return currentPage ? currentPage[1] : null;
};

export const isPageAvailable = ({isMobile, isDesktop}, {isPortrait}) => {
  return (isMobile && isPortrait) || isDesktop;
};

export const fileToDataUrl = (file) => {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = (event) => {
      resolve(event.target!.result);
    };
    reader.onerror = (error) => {
      reject(error);
    };
  });
};

export const downloadFileFromUrl = async (url) => {
  try {
    const response = await fetch(url),
      blob = await response.blob(),
      resultFile = new File([blob], url.split('/').pop(), {type: blob.type}),
      resultData: any = await fileToDataUrl(resultFile),
      resultFileObject: FileObjectWithLinkT = {
        file: resultFile,
        data: resultData,
        link: url,
      };

    return resultFileObject;
  } catch (err) {
    throw new Error(err);
  }
};

export const downloadMedia = async (urlsArr: string[]) => {
  try {
    const promises = urlsArr.map((url) => downloadFileFromUrl(url)),
      results = await Promise.all(promises);

    return results;
  } catch (err) {
    throw new Error(err);
  }
};

export const stringifyObjectInputValue = (values) => (
  Object.keys(values).reduce((resultObj, key) => {
    if (Array.isArray(values[key])) {
      if (values[key][0] && typeof values[key][0] === 'object' && values[key][0].inputValue !== undefined) {
        resultObj[key] = values[key].map((el) => el.inputValue);
      } else {
        resultObj[key] = values[key];
      }
    } else if (typeof values[key] === 'object' && values[key].inputValue !== undefined) {
      resultObj[key] = values[key].inputValue;
    } else {
      resultObj[key] = values[key];
    }

    return resultObj;
  }, {})
);

export const addInitialValueToFields = (fields, currentEntity) => (
  fields.map((field) => {
    if (currentEntity) {
      const currentField = currentEntity[field.key];

      if (currentField) {
        field.initialValue = currentField;
      }
    }

    return field;
  })
);
