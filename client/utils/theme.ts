import {red} from '@material-ui/core/colors';
import {createMuiTheme} from '@material-ui/core/styles';

// Create a theme instance.
const theme = createMuiTheme({
  palette: {
    type: 'dark',
    primary: {
      main: '#1A222C',
      contrastText: '#ffffff',
    },
    secondary: {
      main: '#52A6F8',
      light: '#7aa8f7',
    },
    error: {
      main: red.A400,
    },
    background: {
      default: '#24303F',
      paper: '#30343c',
    },
    text: {
      primary: '#ffffff',
      secondary: '#B4C2D3',
    },
    divider: '#242F3F',
  },
  typography: {
    fontFamily: [
      // 'Poppins',
      'system-ui',
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      'Ubuntu',
      '"Helvetica Neue"',
      'sans-serif',
    ].join(','),
  },
  overrides: {
    MuiButton: {
      label: {
        color: '#ffffff',
      },
    },
  },
});

export default theme;
