import AppModels from '@server/models/AppModels';

export default class Crud {
  protected static modelName = 'Crud'
  protected static populationArr: Array<Object | String | void> = []

  public static async create(obj, onResult?: Function): Promise<Object | null> {
    const Model = AppModels.getModel(this.modelName),
      createdEntity = await new Model(obj).save();
    let entity = null;

    if (createdEntity) {
      entity = await this.getExact({_id: createdEntity.id});
      if (onResult && entity) {
        onResult(entity);
      }
    }

    return entity;
  }

  public static async getAll(filter, onResult?: Function, sort?) {
    const Model = AppModels.getModel(this.modelName),
      options = sort ? {sort} : null;
    let entities = Model.find(filter, null, options);

    entities = await this.populate(entities).lean().exec();
    if (onResult && entities) {
      onResult(entities);
    }

    return entities;
  }

  public static async getExact(filter, onResult?: Function, sort?) {
    const Model = AppModels.getModel(this.modelName),
      options = sort ? {sort} : null;
    let entity = Model.findOne(filter, null, options);

    entity = await this.populate(entity).lean().exec();
    if (onResult && entity) {
      onResult(entity);
    }

    return entity;
  }

  public static async update(filter, updateObj, onResult?: Function, sort?) {
    const Model = AppModels.getModel(this.modelName),
      options = sort ? {sort} : {},
      updateQuery = Object.keys(updateObj).reduce((resQuery, updateKey) => {
        const value = updateObj[updateKey];
        if (value) {
          resQuery.$set[updateKey] = value;
        }

        return resQuery;
      }, {$set: {}}),
      entity = await Model.findOneAndUpdate(filter, updateQuery, {new: true, ...options}).lean().exec();

    if (onResult && entity) {
      onResult(entity);
    }

    return entity;
  }

  public static async delete(filter, onResult?: Function, sort?) {
    const Model = AppModels.getModel(this.modelName),
      options = sort ? {sort} : null,
      entity = await Model.findOneAndRemove(filter, options).lean().exec();

    if (onResult && entity) {
      onResult(entity);
    }

    return entity;
  }

  public static populate(entity) {
    if (this.populationArr?.length) {
      for (const popQuery of this.populationArr) {
        entity = entity.populate(popQuery);
      }
    }

    return entity;
  }
};
