import Crud from '@server/models/Crud';
import {adminEmail} from '@const/serverSettings';

export default class User extends Crud {
  protected static modelName = 'User'

  public static async findOrRegister({email, nickname, name, picture}): Promise<User| null> {
    if (email) {
      const user = await this.getExact({email});

      if (user) {
        return user;
      } else {
        const userObj = {
            email,
            login: nickname,
            name,
            avatar: picture,
            role: email === adminEmail ? 'admin' : 'user',
          },
          newUser = await this.create(userObj);

        return newUser;
      }
    } else {
      return null;
    }
  }
};
