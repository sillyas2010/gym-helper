import Crud from '@server/models/Crud';
import Resolver from '@server/db/Resolver';

export default class Exercise extends Crud {
  protected static modelName = 'Exercise'

  public static async create(obj, onResult?: Function): Promise<Object | null> {
    return await Crud.create.call(this, obj,
      (exercise) => {
        Resolver.getPubSub().publish('newExercise', exercise);

        if (onResult) {
          onResult(exercise);
        }
      },
    );
  }
};
