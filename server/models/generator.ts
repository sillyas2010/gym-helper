import {Schema} from 'mongoose';

interface IField {
  type: {
    ofType: {
      ofType: {
        name: string,
      },
      name: string,
      _values: Array<Object>,
    },
    name: string,
  }
}

interface IEntityTemplate {
  index: Number | any,
  schema: any
}

export function generateSchemas(schema, types, implementation) {
  const arrayOfEntities = schema._implementationsMap[implementation].objects,
    schemasArray: any = [];

  if (arrayOfEntities?.length) {
    const entitiesMap = arrayOfEntities.reduce((resultObject, el, index) => {
      resultObject[el.name] = {schema: {}, index};

      return resultObject;
    }, {});

    for (const entityEntriesArr of Object.entries(entitiesMap)) {
      const entityEntries: any = entityEntriesArr,
        [entityName, entityTemplate]: [string, IEntityTemplate] = entityEntries,
        entityFields = arrayOfEntities[entityTemplate.index]._fields;

      if (types[entityName]?.type === 'skip') {
        continue;
      }

      for (const fieldsEntriesArr of Object.entries(entityFields)) {
        const fieldsEntries: any = fieldsEntriesArr,
          [fieldName, fieldObj]: [string, IField] = fieldsEntries,
          baseFieldTypeObj: any = fieldObj.type.ofType ? fieldObj.type.ofType : {name: fieldObj.type.name},
          nestedFieldTypeObj = baseFieldTypeObj.ofType,
          isFieldArray = !!nestedFieldTypeObj,
          ifFieldEnum = !!baseFieldTypeObj._values,
          currentFieldType: any = isFieldArray ? nestedFieldTypeObj.name : baseFieldTypeObj.name;
        let schemaType: any = {};

        if (types[currentFieldType]) {
          if (types[currentFieldType].type === 'skip') {
            continue;
          } else if (fieldName === '_id' && currentFieldType === 'ID') {
            continue;
          }

          if (ifFieldEnum) {
            const enumValues = baseFieldTypeObj._values.map((el: any) => el.value);

            schemaType = {
              type: types[currentFieldType].enumType,
              enum: enumValues,
              default: enumValues[0],
            };
          } else {
            schemaType = {
              type: types[currentFieldType].type,
            };
          }
        } else {
          schemaType = {
            type: types.__rest__.type,
            ref: currentFieldType,
          };
        }

        if (isFieldArray) {
          schemaType = [schemaType];
        }

        entityTemplate.schema[fieldName] = schemaType;
      }

      schemasArray.push({
        schema: new Schema(entityTemplate.schema, {
          timestamps: {createdAt: 'created_at', updatedAt: 'updated_at'},
        }),
        name: entityName,
      });
    }
  }

  return schemasArray;
}
