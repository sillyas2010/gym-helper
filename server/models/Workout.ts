import Crud from '@server/models/Crud';
import Resolver from '@server/db/Resolver';

export default class Workout extends Crud {
  protected static modelName = 'Workout'
  protected static populationArr = [
    {
      path: 'exercises',
    },
  ]

  public static async create(obj, onResult?: Function): Promise<Object | null> {
    return await Crud.create.call(this, obj,
      (workout) => {
        Resolver.getPubSub().publish('newWorkout', workout);

        if (onResult) {
          onResult(workout);
        }
      },
    );
  }
};
