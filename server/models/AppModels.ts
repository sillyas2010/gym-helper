import {model} from 'mongoose';
import {typeDefs} from '@server/db/schema';
import {generateSchemas} from './generator';
import {types, implementation} from '@const/gQLTypes';

export default class AppModels {
  private static cachedModels: Object = {};

  public static createModel(modelName, modelSchema) {
    const newModel = model(modelName, modelSchema);

    this.cachedModels[modelName] = newModel;

    return newModel;
  }

  public static getModel(modelName) {
    return this.cachedModels[modelName];
  }

  public static getModels() {
    return {...this.cachedModels};
  }

  public static addGraphQLModels() {
    const schemasArray = generateSchemas(typeDefs, types, implementation);

    schemasArray.forEach(
      (shemaObj) => this.createModel(shemaObj.name, shemaObj.schema),
    );

    return {...this.cachedModels};
  }

  public static clearModels() {
    this.cachedModels = {};
  }
}
