import Crud from '@server/models/Crud';
import Resolver from '@server/db/Resolver';

export default class Day extends Crud {
  protected static modelName = 'Day'
  protected static populationArr = [
    {
      path: 'workout',
      populate: {
        path: 'exercises',
      },
    },
  ]

  public static async create(obj, onResult?: Function): Promise<Object | null> {
    return await Crud.create.call(this, obj,
      (day) => {
        Resolver.getPubSub().publish('newDay', day);

        if (onResult) {
          onResult(day);
        }
      },
    );
  }
};
