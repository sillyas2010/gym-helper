import {ApolloServer} from 'apollo-server-express';
import gql from 'graphql-tag';
import mongoose from 'mongoose';

import {gQLPath, isDev} from '@const/serverSettings';
import AppModels from '@server/models/AppModels';

import {context, resolvers, typeDefsStr} from './schema';

export default class DB {
  private static _instance = {}
  private static initiated = false

  private static set instance(instance) {
    if (!this.initiated) {
      this._instance = instance;
      this.initiated = true;
    }
  }

  private static get instance() {
    if (!this.initiated) {
      throw Error('DB in not yet initiated');
    }

    return this._instance;
  }

  public static async getInstance() {
    if (!this.initiated) {
      throw Error('DB in not yet initiated');
    }

    return this.instance;
  }

  public static isInitiated() {
    return this.initiated;
  }

  public static async init(expressServer, connectionURI) {
    if (this.initiated) {
      return this.instance;
    }

    this.instance = await mongoose.connect(connectionURI, {
      autoIndex: true,
      poolSize: 50,
      bufferMaxEntries: 0,
      keepAlive: 120,
      useFindAndModify: false,
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });

    AppModels.addGraphQLModels();

    const apolloServer = new ApolloServer({
      playground: isDev ? {
        settings: {
          'request.credentials': 'include',
        },
      } : false,
      introspection: true,
      tracing: true,
      typeDefs: gql(typeDefsStr),
      resolvers,
      context,
    });

    apolloServer.applyMiddleware({
      app: expressServer,
      path: gQLPath,
      onHealthCheck: () => {
        return new Promise((resolve, reject) => {
          if (mongoose.connection.readyState > 0) {
            resolve();
          } else {
            reject(Error('Not yet connected!'));
          }
        });
      },
    });

    apolloServer.installSubscriptionHandlers(expressServer);

    return this.instance;
  }
}
