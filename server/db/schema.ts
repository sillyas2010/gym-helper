import fs from 'fs';
import {buildSchema} from 'graphql';
import path from 'path';

import Auth from '@server/auth/Auth';
import Day from '@server/models/Day';
import Exercise from '@server/models/Exercise';
import User from '@server/models/User';
import Workout from '@server/models/Workout';

import Resolver from './Resolver';

const schemaString = fs.readFileSync(
    path.join(__dirname, '/../../graphql-schema.graphql'),
    'utf8',
  ),
  typeDefsStr = schemaString,
  typeDefs = buildSchema(schemaString),
  {withAuth} = Resolver,
  requireOwner = true;

const resolvers = {
  Query: {
    exercises: withAuth(async (_, filter) => {
      return await Exercise.getAll(filter);
    }, requireOwner),
    exercise: withAuth(async (_, filter) => {
      return await Exercise.getExact(filter);
    }, requireOwner),
    workouts: withAuth(async (_, filter) => {
      return await Workout.getAll(filter);
    }, requireOwner),
    workout: withAuth(async (_, filter) => {
      return await Workout.getExact(filter);
    }, requireOwner),
    days: withAuth(async (_, filter) => {
      return await Day.getAll(filter);
    }, requireOwner),
    day: withAuth(async (_, filter) => {
      return await Day.getExact(filter);
    }, requireOwner),
    me: async (_, __, {user}) => {
      return user;
    },
  },
  Mutation: {
    addExercise: withAuth(async (_, {title, description, muscleGroup, muscle, imageVideo, owner}) => {
      return await Exercise.create({title, description, muscleGroup, muscle, imageVideo, owner});
    }, requireOwner),
    editExercise: withAuth(async (_, {_id, title, description, muscleGroup, muscle, imageVideo, owner}) => {
      return await Exercise.update({_id, owner}, {title, description, muscleGroup, muscle, imageVideo});
    }, requireOwner),
    removeExercise: withAuth(async (_, {_id, owner}) => {
      return await Exercise.delete({_id, owner});
    }, requireOwner),
    addWorkout: withAuth(async (_, {title, exerciseIds, owner}) => {
      return await Workout.create({title, exercises: exerciseIds, owner});
    }, requireOwner),
    addDate: withAuth(async (_, {date, workoutId, owner}) => {
      return await Day.create({date, workout: workoutId, owner});
    }, requireOwner),
  },
  // Subscription: {
  //   newExercise: setAsyncIterator('newExercise'),
  //   newWorkout: setAsyncIterator('newWorkout'),
  //   newDay: setAsyncIterator('newDay'),
  // },
  Entity: {
    __resolveType() {
      return null;
    },
  },
  Object: {
    __resolveType() {
      return null;
    },
  },
};

const context = async ({req}) => {
  const profile = await Auth.getProfile(req),
    user = profile ? await User.findOrRegister(profile) : null;

  return {user};
};

export {
  typeDefsStr,
  typeDefs,
  resolvers,
  context,
};
