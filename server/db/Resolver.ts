import {PubSub} from 'graphql-subscriptions';

export default class Resolver {
  private static subscription = new PubSub();

  public static getPubSub() {
    return this.subscription;
  }

  public static setAsyncIterator(key) {
    return {
      subscribe: () => Resolver.getPubSub().asyncIterator(key),
    };
  }

  public static withAuth(func, needsOwner?) {
    return async (_, args, ctx, __) => {
      const {user} = ctx;

      if (user) {
        if (needsOwner) {
          args.owner = user._id.toString();
        }

        return func(_, args, ctx, __);
      } else {
        throw Error('Unauthorized');
      }
    };
  }

  public static withAdminAuth(func, needsOwner?) {
    return async (_, args, ctx, __) => {
      const {user} = ctx;

      if (user?.role === 'admin') {
        if (needsOwner) {
          args.owner = user._id.toString();
        }

        return func(_, args, ctx, __);
      } else {
        throw Error('Access Forbidden');
      }
    };
  }
}
