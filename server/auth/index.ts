import {initAuth0} from '@auth0/nextjs-auth0';
import {auth0} from '@const/serverSettings';

const {
  domain,
  clientId,
  clientSecret,
  scope,
  redirectUri,
  postLogoutRedirectUri,
  cookieSecret,
  cookieLifetime,
  httpTimeout,
  clockTolerance,
}: any = auth0;

export default initAuth0({
  domain,
  clientId,
  clientSecret,
  scope,
  redirectUri,
  postLogoutRedirectUri,
  session: {
    cookieSecret,
    cookieLifetime,
  },
  oidcClient: {
    httpTimeout,
    clockTolerance,
  },
});
