import {Router} from 'express';

import {
  authCallbackRoute, authLogoutRoute, authProfileRoute, authRoute,
} from '@const/serverSettings';

import auth0 from './index';

export default class Auth {
  private static async login(req, res) {
    try {
      await auth0.handleLogin(req, res);
    } catch (error) {
      console.error(error);
      res.status(error.status || 400).end(error.message);
    }
  }

  private static async callback(req, res) {
    try {
      await auth0.handleCallback(req, res, {redirectTo: '/'});
    } catch (error) {
      console.error(error);
      res.status(error.status || 400).end(error.message);
    }
  }

  private static async me(req, res) {
    try {
      await auth0.handleProfile(req, res);
    } catch (error) {
      console.error(error);
      res.status(error.status || 500).end(error.message);
    }
  }

  public static async getProfile(req) {
    try {
      const profile: any = await auth0.getSession(req);
      return (profile ? profile.user : null);
    } catch (err) {
      console.warn(err);
      return null;
    }
  }

  public static async logout(req, res) {
    try {
      await auth0.handleLogout(req, res);
    } catch (err) {
      console.warn(err);
      res.status(err.status || 400).end(err.message);
    }
  }

  public static init() {
    // eslint-disable-next-line new-cap
    const router = Router();

    router.get(authRoute, Auth.login);
    router.get(authLogoutRoute, Auth.logout);
    router.get(authProfileRoute, Auth.me);
    router.get(authCallbackRoute, Auth.callback);

    return router;
  }
};
