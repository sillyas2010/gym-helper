import {rawHost, host, publicRoute} from '@const/serverSettings';
import corsInit from 'cors';

export const cors = () => {
  const whitelist = [rawHost, host],
    corsOptions: any = (req, callback) => {
      const host = req.header('Origin') ?
        req.header('Origin') :
        (req.header('Host') ? req.header('Host') : null);

      if (whitelist.indexOf(host) !== -1) {
        callback(null, {origin: true});
      } else {
        callback(new Error('Not allowed by CORS'));
      }
    };

  return corsInit(corsOptions);
};

export const forceHttps = (req, res, next) => {
  const proto = req.headers['x-forwarded-proto'];

  if (proto === 'https') {
    res.set({
      'Strict-Transport-Security': 'max-age=31557600',
    });
    return next();
  }
  res.redirect('https://' + req.headers.host + req.url);
};

export const errorHandler = (err, req, res, next) => {
  if (err) {
    res.status(500);
    res.json({error: err.message});
  } else {
    next(err, req, res, next);
  }
};

export const bindArgs = function(func, ...boundArgs) {
  return function(...args) {
    return func.call(func, ...boundArgs, ...args);
  };
};

export const serveSW = function(req, res) {
  res.sendfile(req.path.substring(1), {root: publicRoute.substring(1)});
};
