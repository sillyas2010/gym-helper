import cluster from 'cluster';
import express from 'express';
import next from 'next';
import os from 'os';
import {promisify} from 'util';

import {
  allRoute, apiPort, authPath, dbUri, host, isDev, publicRoute, swRoute, wbRoute, wsHost,
} from '@const/serverSettings';

import Auth from './auth/Auth';
import DB from './db/DB';
import {cors, errorHandler, forceHttps, serveSW} from './utils';

async function main() {
  const expressServer = express(),
    appPort: any = apiPort;

  if (!isDev) {
    expressServer.all(allRoute, forceHttps);
    expressServer.all(allRoute, cors());
  }

  expressServer.use(authPath, Auth.init());

  await DB.init(expressServer, dbUri);

  const nextServer = next({dev: isDev});
  await nextServer.prepare();
  const nextServerRequestHandler: any = nextServer.getRequestHandler();

  expressServer.use(publicRoute, express.static('public'));
  expressServer.get(swRoute, serveSW);
  expressServer.get(wbRoute, serveSW);
  expressServer.all(allRoute, nextServerRequestHandler);
  expressServer.use(errorHandler);

  const listenFn: (port: number) => void = promisify(
    expressServer.listen.bind(expressServer),
  );

  await listenFn(appPort);

  console.log(`> WsServer Ready on ${wsHost}`);
  console.log(`> ApiServer Ready on ${host}`);
}

async function init() {
  const numCPUs = os.cpus().length;

  if (!isDev && cluster.isMaster) {
    console.log(`Node cluster master ${process.pid} is running`);

    // Fork workers.
    for (let i = 0; i < numCPUs; i++) {
      cluster.fork();
    }

    cluster.on('exit', (worker, code, signal) => {
      console.error(`Node cluster worker ${worker.process.pid} exited: code ${code}, signal ${signal}`);
    });
  } else {
    await main();
  }
};

init().catch((error) => {
  console.error(error);
  process.exit(1);
});
