const path = require('path'),
  withPWA = require('next-pwa'),
  {
    PORT,
    NODE_ENV,
  } = process.env,
  {
    isDev,
    sourseMapDevTool: devtool,
    publicRoute,
    swRoute,
  } = require('@const/serverSettings'),
  {
    webpack: webpackWithDevtools,
  } = require('@zeit/next-source-maps')();

const nextConfig = {
  publicRuntimeConfig: {
    PORT,
    NODE_ENV,
  },
  pwa: {
    disable: isDev,
    dest: publicRoute.substring(1),
    sw: swRoute.substring(1),
  },
  webpack: (config, props) => {
    if (isDev) {
      config = webpackWithDevtools(config, props);
      config.devtool = devtool;
    }
    config.resolve.alias['@'] = path.resolve(path.join(__dirname));
    config.resolve.alias['@const'] = path.resolve(path.join(__dirname, '/const'));
    config.resolve.alias['@server'] = path.resolve(path.join(__dirname, '/server'));
    config.resolve.alias['@client'] = path.resolve(path.join(__dirname, '/client'));
    config.resolve.alias['@components'] = path.resolve(path.join(__dirname, '/client/components'));
    return config;
  },
};

global.File = typeof window === 'undefined' ? Object : window.File;
global.FileList = typeof window === 'undefined' ? Object : window.FileList;

module.exports = withPWA(nextConfig);
