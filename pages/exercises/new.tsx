import {useRouter} from 'next/router';
import {useMemo} from 'react';

import {useMutation, useQuery} from '@apollo/react-hooks';
import {addExercise, exercisesList} from '@client/apollo/queries';
import withApollo from '@client/hoc/withApollo';
import Form from '@components/Form/Form';
import {AutocompleteVariantT, ExerciseT, FormFieldT} from '@const/types';
import {Container} from '@material-ui/core';

const formFields: FormFieldT[] = [
  {
    id: 'title',
    key: 'title',
    label: 'Title',
    required: true,
    validate: (values, key) => {
      const currentField = values[key];
      if (currentField.length <= 3) {
        return 'Too Short';
      }
    },
  },
  {
    id: 'description',
    key: 'description',
    label: 'Description',
  },
  {
    id: 'muscleGroup',
    key: 'muscleGroup',
    label: 'Muscle Group',
    required: true,
    creatable: true,
    type: 'autocomplete',
    variants: [],
  },
  {
    id: 'muscle',
    key: 'muscle',
    required: true,
    label: 'Muscle',
    type: 'select',
    variants: [],
  },
  {
    id: 'imageVideo',
    key: 'imageVideo',
    label: 'Image/Video',
    initialValue: [],
    type: 'media',
    required: true,
    multiple: true,
    limit: 5,
    accepted: [
      'image/*',
      'video/mp4',
      'video/webm',
      'video/x-matroska',
      'video/quicktime',
      'video/x-flv',
      'video/x-msvideo',
      'video/x-ms-wmv',
      'video/mpeg',
    ],
  },
],
  muscleGroupInd = formFields.findIndex((field) => field.key === 'muscleGroup'),
  muscleInd = formFields.findIndex((field) => field.key === 'muscle'),
  getUniqueMuscleArrays = (exercises: ExerciseT[]) => {
    const muscles: string[] = [],
      muscleGroups: string[] = [];
    exercises.forEach((exercise: ExerciseT) => {
      if (exercise.muscleGroup) {
        muscleGroups.push(exercise.muscleGroup);
      }
      if (exercise.muscle) {
        muscles.push(exercise.muscle);
      }
    });

    const uniqueMuscles: AutocompleteVariantT[] =
        [...new Set(muscles)].map((el) => ({label: el, inputValue: el})),
      uniqueMuscleGroups: AutocompleteVariantT[] =
        [...new Set(muscleGroups)].map((el) => ({label: el, inputValue: el}));

    return [uniqueMuscles, uniqueMuscleGroups];
  };

const ExerciseNew = () => {
  const {loading, data, refetch} = useQuery(exercisesList),
    [addExerciseM] = useMutation(addExercise),
    router = useRouter(),
    exercises = !loading && data ? data.exercises : [],
    [muscleGroupVariants, musclesVariants] = useMemo(() => getUniqueMuscleArrays(exercises), [exercises]),
    onSubmit = async (values) => {
      try {
        const mutationRes = await addExerciseM({variables: values});
        if (mutationRes.errors) {
          console.error(mutationRes.errors);
          return {error: mutationRes.errors.toString()};
        } else {
          await refetch();
          setTimeout(() => router.push('/exercises'), 500);
          return {message: 'Submitted successfully', data: mutationRes.data};
        }
      } catch (err) {
        console.error(err);
        return {error: err.toString()};
      }
    };

  formFields[muscleGroupInd].variants = muscleGroupVariants;
  formFields[muscleInd].variants = musclesVariants;

  return (
    <Container maxWidth="sm">
      <Form formFields={formFields} title="New Exercise" onSubmit={onSubmit}/>
    </Container>
  );
};

export default withApollo({ssr: true})(ExerciseNew);
