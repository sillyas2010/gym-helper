import {useRouter} from 'next/router';
import NProgress from 'nprogress';
import {useEffect, useMemo, useState} from 'react';

import {useMutation, useQuery} from '@apollo/react-hooks';
import {editExercise, exercisesList} from '@client/apollo/queries';
import withApollo from '@client/hoc/withApollo';
import {addInitialValueToFields, downloadMedia} from '@client/utils';
import ClientOnly from '@components/ClientOnly/ClientOnly';
import Form from '@components/Form/Form';
import {AutocompleteVariantT, DownloadFilesUseStateT, ExerciseT, FormFieldT} from '@const/types';
import {Container} from '@material-ui/core';

const formFields: FormFieldT[] = [
  {
    id: 'title',
    key: 'title',
    label: 'Title',
    required: true,
    validate: (values, key) => {
      const currentField = values[key];
      if (currentField.length <= 3) {
        return 'Too Short';
      }
    },
  },
  {
    id: 'description',
    key: 'description',
    label: 'Description',
  },
  {
    id: 'muscleGroup',
    key: 'muscleGroup',
    label: 'Muscle Group',
    required: true,
    creatable: true,
    type: 'autocomplete',
    variants: [],
  },
  {
    id: 'muscle',
    key: 'muscle',
    required: true,
    label: 'Muscle',
    type: 'select',
    variants: [],
  },
  {
    id: 'imageVideo',
    key: 'imageVideo',
    label: 'Image/Video',
    initialValue: [],
    type: 'media',
    required: true,
    multiple: true,
    limit: 5,
    accepted: [
      'image/*',
      'video/mp4',
      'video/webm',
      'video/x-matroska',
      'video/quicktime',
      'video/x-flv',
      'video/x-msvideo',
      'video/x-ms-wmv',
      'video/mpeg',
    ],
  },
],
  muscleGroupInd = formFields.findIndex((field) => field.key === 'muscleGroup'),
  muscleInd = formFields.findIndex((field) => field.key === 'muscle'),
  mediaIndex = formFields.findIndex((field) => field.key === 'imageVideo'),
  getUniqueMuscleArrays = (exercises: ExerciseT[]) => {
    const muscles: string[] = [],
      muscleGroups: string[] = [];
    exercises.forEach((exercise: ExerciseT) => {
      if (exercise.muscleGroup) {
        muscleGroups.push(exercise.muscleGroup);
      }
      if (exercise.muscle) {
        muscles.push(exercise.muscle);
      }
    });

    const uniqueMuscles: AutocompleteVariantT[] =
        [...new Set(muscles)].map((el) => ({label: el, inputValue: el})),
      uniqueMuscleGroups: AutocompleteVariantT[] =
        [...new Set(muscleGroups)].map((el) => ({label: el, inputValue: el}));

    return [uniqueMuscleGroups, uniqueMuscles];
  };

const ExerciseEdit = () => {
  const {loading, data, refetch} = useQuery(exercisesList),
    [editExerciseM] = useMutation(editExercise),
    router = useRouter(),
    {id} = router.query,
    hasLoaded = !loading && data,
    exercises = hasLoaded ? data.exercises : [],
    currentExercise = exercises ? exercises.find((exercise) => exercise._id === id) : null,
    [{isDownloading, downloadedFiles}, setFiles]: DownloadFilesUseStateT =
      useState({isDownloading: false, downloadedFiles: []}),
    [muscleGroupVariants, musclesVariants] = useMemo(() => getUniqueMuscleArrays(exercises), [exercises]),
    onSubmit = async (values) => {
      try {
        const mutationRes = await editExerciseM({variables: {...values, _id: id}});
        if (mutationRes.errors) {
          console.error(mutationRes.errors);
          return {error: mutationRes.errors.toString()};
        } else {
          await refetch();
          setTimeout(() => router.push('/exercises'), 500);
          return {message: 'Submitted successfully', data: mutationRes.data};
        }
      } catch (err) {
        console.error(err);
        return {error: err.toString()};
      }
    },
    resultFormFields = addInitialValueToFields(formFields, currentExercise);

  useEffect(() => {
    if (!currentExercise) {
      NProgress.start();
    }
    if (currentExercise && !isDownloading && !downloadedFiles.length) {
      setFiles({isDownloading: true, downloadedFiles: []});
      (async () => {
        const files = await downloadMedia(currentExercise.imageVideo);

        setFiles({isDownloading: false, downloadedFiles: files});
        NProgress.done();
      })();
    }
  }, [currentExercise, downloadedFiles.length, isDownloading]);

  resultFormFields[muscleGroupInd].variants = muscleGroupVariants;
  resultFormFields[muscleInd].variants = musclesVariants;
  resultFormFields[mediaIndex].initialValue = downloadedFiles;

  return (
    <Container maxWidth="sm">
      <ClientOnly>
        { hasLoaded && currentExercise && downloadedFiles && downloadedFiles.length ?
          <Form formFields={formFields} title={`Edit Exercise "${currentExercise.title}"`}
            onSubmit={onSubmit} isTouched={true}
          /> :
          ''
        }
      </ClientOnly>
    </Container>
  );
};

export default withApollo({ssr: true})(ExerciseEdit);
