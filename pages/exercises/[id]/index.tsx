import {useRouter} from 'next/router';
import NProgress from 'nprogress';
import {useEffect} from 'react';

import {useLazyQuery, useMutation, useQuery} from '@apollo/react-hooks';
import {exerciseDetail, exercisesList, removeExercise} from '@client/apollo/queries';
import EntityDetail from '@components/EntityDetail/EntityDetail';

const ExerciseDetailPage = () => {
  const {query} = useRouter(),
    [loadExercise, {loading, data, called}] = useLazyQuery(exerciseDetail),
    exercise = !loading && data ? data.exercise || {} : {},
    coverImage = exercise && Array.isArray(exercise.imageVideo) ? exercise.imageVideo[0] : '';

  useEffect(() => {
    if (query.id && !called) {
      loadExercise({variables: {_id: query.id}});
    }
  }, [query.id, called]);

  useEffect(() => {
    if (loading) {
      NProgress.start();
    } else {
      NProgress.done();
    }
  }, [loading]);

  return (
    <EntityDetail entity={exercise} type="Exercise" image={coverImage}/>
  );
};

export default ExerciseDetailPage;
