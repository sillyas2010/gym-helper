import Exercises from '@components/Exercises/Exercises';

import withApollo from '@client/hoc/withApollo';

import React from 'react';

function ExercisesIndex() {
  return (
    <Exercises/>
  );
};

export default withApollo({ssr: true})(ExercisesIndex);
