import Router from 'next/router';
import React, {useEffect} from 'react';

import {useQuery} from '@apollo/react-hooks';
import {currentUser} from '@client/apollo/queries';
import withApollo from '@client/hoc/withApollo';
import Hidden from '@components/Hidden/Hidden';
import {pagesByKey} from '@const/clientSettings';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';

const Profile = () => {
  const {loading, data} = useQuery(currentUser),
    signedIn = !loading && data && data.me !== null,
    onLogOut = () => Router.push(pagesByKey.logout);


  useEffect(() => {
    if (!loading && !signedIn) {
      Router.push(pagesByKey.login);
    }
  }, [loading]);

  return (
    <Container maxWidth="sm">
      <Box>
        <Hidden condition={!signedIn}>
          {
            <>
              <Typography variant="h4" component="h1" gutterBottom>
                Email - {data?.me?.email} <br/>
                Name - {data?.me?.name}
              </Typography>
              <Button variant="contained" color="primary" onClick={onLogOut}>
                Logout
              </Button>
            </>
          }
        </Hidden>
      </Box>
    </Container>
  );
};

export default withApollo()(Profile);
