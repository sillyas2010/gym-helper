import 'nprogress/nprogress.css';

import {AppProps} from 'next/app';
import Router from 'next/router';
import NProgress from 'nprogress';
import React, {useEffect} from 'react';

import theme from '@client/utils/theme';
import HeadContent from '@components/HeadContent/HeadContent';
import Page from '@components/Page/Page';
import {pages} from '@const/clientSettings';
import CssBaseline from '@material-ui/core/CssBaseline';
import {ThemeProvider} from '@material-ui/core/styles';

Router.events.on('routeChangeStart', () => NProgress.start());
Router.events.on('routeChangeComplete', () => NProgress.done());
Router.events.on('routeChangeError', () => NProgress.done());

const MyApp = ({Component, pageProps, router}: AppProps) => {
  useEffect(() => {
    const jssStyles: any = document.querySelector('#jss-server-side');
    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles);
    }
  }, []);

  const currentPage = pages.get(router.pathname) || pages.get('/_error');

  return (
    <>
      <HeadContent title={currentPage!.title}/>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <Page router={router}>
          <Component {...pageProps} router={router} />
        </Page>
      </ThemeProvider>
    </>
  );
};

export default MyApp;
